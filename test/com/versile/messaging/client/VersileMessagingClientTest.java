/*
 * Copyright (c) 2012 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.versile.Versile;
import org.versile.common.auth.VPrivateCredentials;
import org.versile.crypto.VCryptoException;
import org.versile.crypto.VDecentralIdentity;
import org.versile.orb.entity.VCallError;
import org.versile.orb.url.VUrlException;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class VersileMessagingClientTest {
    
    File messagesDir = new File("versilemessagestorage");
    VPrivateCredentials credentials;
    Process process = null;
    
    public VersileMessagingClientTest() {
        try {
            credentials = new VPrivateCredentials(VDecentralIdentity.dia(1024, "test", "peter", "dfw/23hcFDawqaDreq"));
        } catch (VCryptoException ex) {
            Logger.getLogger(VersileMessagingClientTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(!messagesDir.exists()) {
            messagesDir.mkdir();
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
        // Versile license 
        Versile.setInternalUseAGPL();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        if(process!=null) {
            process.destroy();
            process = null;
        }
    }
    
    @Test
    public void testWithServer() throws Exception {
        System.out.println("Test with server");
        
        ProcessBuilder pb = new ProcessBuilder(new String[] {
            "java",
            "-cp",
            "build/classes:lib/versile-java-0.8.1-beta.jar",
            "com.versile.messaging.central.VersileMessaging"
        });
        pb.redirectError(Redirect.INHERIT);
        process = pb.start();
        System.out.println("Server started in separate process");

        System.out.println("Creating file 1");

        File testFile = new File(messagesDir,"TestFile1");
        FileOutputStream fos = new FileOutputStream(testFile);
        int checksum1 = 0;
        for(int n=0;n<2*1024*1024;n++) {
            // 2 MB 
            byte b = (byte)(256*Math.random());
            checksum1+=b;
            fos.write(b);
            if(n%(1024*1024)==0) {
                System.out.println(n);
            }
        }
        fos.close();
        
        assertTrue(checksum1!=0);
        
        System.out.println("Creating file 2");
        
        testFile = new File(messagesDir,"TestFile2");
        fos = new FileOutputStream(testFile);
        int checksum2 = 0;
        for(int n=0;n<1*1024*1024;n++) {
            // 1 MB 
            byte b = (byte)(256*Math.random());
            checksum2+=b;
            fos.write(b);
            if(n%(1024*1024)==0) {
                System.out.println(n);
            }
        }
        fos.close();        
        
        assertTrue(checksum2!=0);
        
        System.out.println("Created files");
        
        VersileMessagingClient client = new VersileMessagingClient("localhost");
        client.connect(credentials);
        String[] messages = client.listMessages();
        HashSet<String> messagesSet = new HashSet<String>();
        for(String message : messages) {
            messagesSet.add(message);
        }
        assertTrue(messagesSet.contains("TestFile1"));
        assertTrue(messagesSet.contains("TestFile2"));
        
        FileInputStream fis = new FileInputStream(client.getMessage("TestFile1"));
        int verifySum1 = 0;
        int b = fis.read();
        while(b!=-1) {
            verifySum1+=(byte)b;
            b = fis.read();
        }
        assertEquals(checksum1,verifySum1);
        
        client.deleteMessage("TestFile1");
        assertTrue(!new File(messagesDir,"TestFile1").exists());
        
        fis = new FileInputStream(client.getMessage("TestFile2"));
        int verifySum2 = 0;
        b = fis.read();
        while(b!=-1) {
            verifySum2+=(byte)b;
            b = fis.read();
        }
        assertEquals(checksum2,verifySum2);
        client.deleteMessage("TestFile2");
        assertTrue(!new File(messagesDir,"TestFile2").exists());
        
        process.destroy();
        process=null;
    }
    
    @Test
    public void testSendMessageAsStream() throws Exception {
        System.out.println("Test send message as stream");
        
        ProcessBuilder pb = new ProcessBuilder(new String[] {
            "java",
            "-cp",
            "build/classes:build/test/classes:lib/versile-java-0.8.1-beta.jar",
            "com.versile.messaging.central.VersileMessagingTestServer"
        });
        pb.redirectError(Redirect.INHERIT);
        pb.redirectOutput(Redirect.INHERIT);
        
        process = pb.start();
        System.out.println("Server started in separate process");
        Thread.sleep(5000);
        
        System.out.println("Creating client");
        VersileMessagingClient client = new VersileMessagingClient("localhost");
        client.connect(credentials);
        System.out.println("Client connected");
        OutputStream os = client.sendMessageAsStream("testSendStreamMessage");
        System.out.println("Send message stream obtained");
        int checksum1 = 0;
        for(int n=0;n<100*1024*1024;n++) {
            // 100 MB 
            byte b = (byte)(256*Math.random());
            checksum1+=b;
            os.write(b);
            if(n%(1024*1024)==0) {
                System.out.println(n);
            }
        }
        os.close();
        System.out.println("Message written");
        
        assertTrue(checksum1!=0);
        
        assertTrue(new File(messagesDir,"testSendStreamMessage").exists());

        client.notifyListeners("testSendStreamMessage");
        System.out.println("Clients notified");
        
        
        Thread.sleep(500);
        System.out.println("Check notification message");
        
        File f = client.getMessage("msgreceived.txt");
        assertTrue(f.exists());

        
        client.deleteMessage("msgreceived.txt");

        assertTrue(!new File(messagesDir,"msgreceived.txt").exists());

        FileInputStream fis = new FileInputStream(client.getMessage("testSendStreamMessage"));
        int verifySum1 = 0;
        int b = fis.read();
        while(b!=-1) {
            verifySum1+=(byte)b;
            b = fis.read();
        }
        assertEquals(checksum1,verifySum1);
        fis.close();
                        
        client.deleteMessage("testSendStreamMessage");
        assertTrue(!new File(messagesDir,"testSendStreamMessage").exists());
                
        process.destroy();
        process=null;
    }
    
    @Test
    public void testSendMessage() throws Exception {
        System.out.println("Test send message");
        
        ProcessBuilder pb = new ProcessBuilder(new String[] {
            "java",
            "-cp",
            "build/classes:build/test/classes:lib/versile-java-0.8.1-beta.jar",
            "com.versile.messaging.central.VersileMessagingTestServer"
        });
        pb.redirectError(Redirect.INHERIT);
        pb.redirectOutput(Redirect.INHERIT);
        
        process = pb.start();
        System.out.println("Server started in separate process");
        Thread.sleep(5000);
        
        System.out.println("Creating client");
        VersileMessagingClient client = new VersileMessagingClient("localhost");
        client.connect(credentials);
        client.sendMessage("test", "Dette er en test".getBytes());
        File f = client.getMessage("test");
        FileReader fr = new FileReader(f);
        char[] cbuf = new char["Dette er en test".length()];
        fr.read(cbuf);
        assertEquals("Dette er en test",new String(cbuf));
        client.deleteMessage("test");
        assertTrue(!new File(messagesDir,"test").exists());
    }
    
    /**
     * Test of connect method, of class VersileMessagingClient.
     */
    @Test(timeout=10000)
    public void testConnectWithNoServer() throws Exception {
        System.out.println("connect with no server");
        VersileMessagingClient instance = new VersileMessagingClient("localhost");
        for(int n=0;n<50;n++) {
            System.out.println("Connect attempt "+n);
            try {
                instance.connect(credentials);        
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    @Test
    public void testLooseConnection() throws Exception {
        ProcessBuilder pb = new ProcessBuilder(new String[] {
            "java",
            "-cp",
            "build/classes:build/test/classes:lib/versile-java-0.8.1-beta.jar",
            "com.versile.messaging.central.VersileMessagingTestServer"
        });
        pb.redirectError(Redirect.INHERIT);
        pb.redirectOutput(Redirect.INHERIT);
        
        process = pb.start();
        System.out.println("Server started in separate process");
        Thread.sleep(5000);
        
        System.out.println("Creating client");
        VersileMessagingClient client = new VersileMessagingClient("localhost");
        client.connect(credentials);
        client.sendMessage("test", "Dette er en test".getBytes());
        
        client.getMessage("test");
        process.destroy();
        process.waitFor();
        
        System.out.println("Server killed - trying to connect");
        try {
            System.out.println("Have experienced deadlocks here (see versiledeadlock.txt)");
            client.getMessage("test");
            fail("Should not happen");
        } catch(Exception e) {
            
        }
               
        System.out.println("Starting server");
        process = pb.start();
        
        Thread.sleep(5000);
        System.out.println("Server started");
        
        client = new VersileMessagingClient("localhost");
        client.connect(credentials);
        File f = client.getMessage("test");
        
        FileReader fr = new FileReader(f);
        char[] cbuf = new char["Dette er en test".length()];
        fr.read(cbuf);
        assertEquals("Dette er en test",new String(cbuf));
        client.deleteMessage("test");
        assertTrue(!new File(messagesDir,"test").exists());
    }
    
    @Test(timeout=20000L)
    public void testNotifyClientListeners() throws Exception {
        ProcessBuilder pb = new ProcessBuilder(new String[] {
            "java",
            "-cp",
            "build/classes:build/test/classes:lib/versile-java-0.8.1-beta.jar",
            "com.versile.messaging.central.VersileMessagingTestServer"
        });
        pb.redirectError(Redirect.INHERIT);
        pb.redirectOutput(Redirect.INHERIT);
        
        process = pb.start();
        System.out.println("Server started in separate process");
        Thread.sleep(5000);
        
        final boolean[] clientListenerCalled = new boolean[] {false};
        
        System.out.println("Creating client");
        VersileMessagingClient client = new VersileMessagingClient("localhost");
        client.connect(credentials);
        client.enableClientListeners();
        
        class CL implements ClientListener {
            @Override
            public synchronized void notifyClient(String filename) {
                clientListenerCalled[0] = true;
                System.out.println(filename);
                notifyAll();
            }            
            
            public synchronized void waitNotification() throws InterruptedException {
                wait();
            }
        };
        CL cl = new CL();
        client.addClientListener(cl);
        cl.waitNotification();
        assertTrue(clientListenerCalled[0]);
    }
}
