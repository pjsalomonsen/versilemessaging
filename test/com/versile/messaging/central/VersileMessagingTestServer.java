/*
 * Copyright (c) 2012 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.versile.messaging.central;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.versile.Versile;
import org.versile.crypto.VDecentralIdentity;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class VersileMessagingTestServer {
    public static void main(String[] args) throws Exception {
        Versile.setInternalUseAGPL();
        
        final VersileMessagingVOPServiceManager vmm = new VersileMessagingVOPServiceManager(VDecentralIdentity.dia(1024, "VersileMessaging", "Versile", "Versile2014"),
                new FileInputStream("authorized_keys"));
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                Logger.getLogger(getClass().getName()).log(Level.INFO,"Received kill signal - will stop");
                vmm.stop();
                Logger.getLogger(getClass().getName()).log(Level.INFO,"Versile messaging terminated");
            }
            
        });
        vmm.start();
        vmm.getLocalMessageNotification().registerLocalMessageListener(new LocalMessageListener() {

            @Override
            public void messageReceived(String filename) {
                FileOutputStream fos = null;
                try {
                    System.out.println("Message "+filename+" received");
                    
                    fos = new FileOutputStream(new File(vmm.getMessagesDir(),"msgreceived.txt"));
                    fos.write(("Message "+filename+" received").getBytes());
                    fos.close();
                    System.out.println("Notification message written");
                } catch (Exception ex) {
                    Logger.getLogger(VersileMessagingTestServer.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        fos.close();
                    } catch (IOException ex) {
                        Logger.getLogger(VersileMessagingTestServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        });
        Logger.getLogger(VersileMessaging.class.getName()).log(Level.INFO,"Versile messaging started");
        System.out.println("Versile messaging started");
        Thread.sleep(10000);
        vmm.notifyClientListeners("blabla");
        System.out.println("Client listeners notified");
    }
}
