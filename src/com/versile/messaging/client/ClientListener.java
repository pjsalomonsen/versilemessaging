/*
 * Copyright (c) 2012 Peter Johan Salomonsen
 */
package com.versile.messaging.client;

/**
 *
 * @author Peter Johan Salomonsen
 */
public interface ClientListener {
    public void notifyClient(String filename);
}
