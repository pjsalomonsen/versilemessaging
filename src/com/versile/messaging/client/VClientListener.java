/*
 * Copyright (c) 2012 Peter Johan Salomonsen
 */
package com.versile.messaging.client;

import org.versile.orb.external.Publish;
import org.versile.orb.external.VExternal;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class VClientListener extends VExternal {

    VersileMessagingClient client;

    public VClientListener(VersileMessagingClient client) {
        this.client = client;
    }        
   
    @Publish(show=true,ctx=false)
    public void notifyClientListener(String filename) {
        client.notifyClientListeners(filename);
    }
}
