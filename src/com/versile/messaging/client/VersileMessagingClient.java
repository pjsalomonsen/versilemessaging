/*
 * Copyright (c) 2012 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.versile.common.auth.VPrivateCredentials;
import org.versile.orb.entity.VProxy;
import org.versile.orb.link.VLinkReference;
import org.versile.orb.url.VUrlException;
import org.versile.reactor.url.VUrl;
import org.versile.vse.VSEResolver;
import org.versile.vse.stream.VByteStream;
import org.versile.vse.stream.VByteStreamerProxy;
import org.versile.vse.stream.VStream;
import org.versile.vse.stream.VStreamError;

/**
 * Client for Versile Messaging
 * 
 * @author Peter Johan Salomonsen
 */
public class VersileMessagingClient {

    static {
        VSEResolver.addToGlobalModules();
    }

    String hostname;
    VProxy proxy;
    VPrivateCredentials credentials;
    
    HashSet<ClientListener> clientListeners = new HashSet<ClientListener>();
    ExecutorService callbackExec = Executors.newFixedThreadPool(4);
    
    public VersileMessagingClient(String hostname) {
        this.hostname = hostname;
    }
    
    public void connect(VPrivateCredentials credentials) throws VUrlException {
        this.credentials = credentials;
        proxy = (VProxy) VUrl.resolve("vop://"+hostname+"/VersileMessaging/",credentials);
        
    }
    
    private void reconnect() {
        disconnect();
        try {
            connect(credentials);
        } catch (VUrlException ex) {
            Logger.getLogger(VersileMessagingClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void enableClientListeners() throws Exception {
        proxy.call("registerClientListener",new VClientListener(this));
    }
    
    public String[] listMessages() throws Exception {
        Object[] fileNames = (Object[]) proxy.call("listMessages");
        ArrayList<String> fileNamesList = new ArrayList<String>();
        for(Object fileName : fileNames) {
            fileNamesList.add(""+fileName);
        }
        return fileNamesList.toArray(new String[0]);
    }
    
    /**
     * Write a message to the specified file
     * @param messageFileName
     * @param destFile
     * @throws Exception 
     */
    public void saveMessageToFile(String messageFileName,File destFile) throws Exception {
        VByteStreamerProxy streamer = (VByteStreamerProxy) proxy.call("getMessage", messageFileName);
        VByteStream stream = streamer.connect();
        stream.waitActive();
        //stream.enableReadahead(); // Improves stream read performance
        
        FileOutputStream fos = new FileOutputStream(destFile);
        byte[] filedata;
        do {
            filedata = stream.read(65536);            
            fos.write(filedata);
        } while(filedata.length>0);
        fos.close();
        try {
            // Sometimes stream is not able to close - so as a workaround catch the close exception and try again
            stream.close();
        } catch(Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "Unable to close stream attempts - will retry",e);
            if(!stream.isClosed()) {
                int n = 0;
                for(n=0;n<5 && !stream.isClosed();n++) {
                    try {
                        Thread.sleep(500);
                        stream.close();
                    } catch(Exception ex) {
                        e = ex;
                    }
                }
                if(!stream.isClosed()) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Unable to close stream after "+n+" attempts - will try reconnect",e);
                    reconnect();
                } else {
                    Logger.getLogger(getClass().getName()).log(Level.WARNING, "Took {0} attempts to close stream", n);
                }
            }
        }     
    }
    
    /**
     * Write message to a file in the system tmp dir and return the written file. The file will be deleted on exit
     * 
     * @param filename
     * @return
     * @throws Exception 
     */
    public File getMessage(String filename) throws Exception {        
        File file = new File(System.getProperty("java.io.tmpdir")+"/"+filename);
        file.deleteOnExit();
        saveMessageToFile(filename, file);
        return file;
    }
    
    public OutputStream sendMessageAsStream(String filename) throws Exception {
        VByteStreamerProxy streamer = (VByteStreamerProxy) proxy.call("sendMessageAsStream", filename);
        final VByteStream stream = streamer.connect();        
        stream.waitActive();        
        stream.seek(0, VStream.PosBase.START);        
        return new OutputStream() {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            @Override
            public void write(int b) throws IOException {
                baos.write(b);
                if(baos.size()>=8*1024) {
                    flush();
                }
            }

            @Override
            public void flush() throws IOException {
                try {
                    stream.write(baos.toByteArray());
                    //stream.seek(0, VStream.PosBase.CURRENT);
                    baos.reset();
                } catch (VStreamError ex) {
                    Logger.getLogger(VersileMessagingClient.class.getName()).log(Level.SEVERE, null, ex);
                    throw new IOException(ex);
                }
            }               

            @Override
            public void close() throws IOException {
                flush();                                
                stream.close();
                super.close();
            }                        
        };
    }
    
    public void sendMessage(String filename,byte[] msgData) throws Exception {
        proxy.call("sendMessage", filename,msgData);
    }
    
    public void notifyListeners(String filename) throws Exception {
        proxy.call("notifyListeners",filename);
    }
    
    public void deleteMessage(String filename) throws Exception {
        proxy.call("deleteMessage", filename);        
        try {
            new File(System.getProperty("java.io.tmpdir")+"/"+filename).delete();
        } catch(Exception e) {}
    }
    
    public void registerClientListener(ClientListener clientListener) throws Exception {
        
    }
    
    public void disconnect() {
        ((VLinkReference)proxy.get())._v_link().shutdown(false);
    }        

    public void notifyClientListeners(final String filename) {
        for(final ClientListener cl : clientListeners.toArray(new ClientListener[0])) {
            callbackExec.submit(new Runnable() {
                @Override
                public void run() {
                    cl.notifyClient(filename);
                }
            });
        }
    }
    
    public void addClientListener(ClientListener clientListener) {
        clientListeners.add(clientListener);
    }
    
    public void removeClientListener(ClientListener clientListener) {
        clientListeners.remove(clientListener);
    }
}
