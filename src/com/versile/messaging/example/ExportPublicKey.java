/*
 * Copyright (c) 2012-2014 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.versile.messaging.example;

import org.versile.common.auth.VPrivateCredentials;
import org.versile.crypto.VDecentralIdentity;
import org.versile.crypto.VRSAKeyPair;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class ExportPublicKey {
    public static void main(String[] args) throws Exception {
        VRSAKeyPair key = VDecentralIdentity.dia(1024, "test", "peter", "dfw/23hcFDawqaDreq");        
        System.out.println(new String(key.exportPublicArmoredPkcs()));
    }
}
