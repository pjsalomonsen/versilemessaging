/*
 * Copyright (c) 2012-2014 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.versile.messaging.example;

import com.versile.messaging.client.VersileMessagingClient;
import java.io.File;
import org.versile.Versile;
import org.versile.common.auth.VPrivateCredentials;
import org.versile.crypto.VDecentralIdentity;
import org.versile.crypto.VRSAKeyPair;

/**
 * Example of connecting to Versile messaging. Remember to start com.versile.messaging.central.VersileMessaging at
 * the destination server
 * 
 * @author Peter Johan Salomonsen
 */
public class ExampleClient {
    public static void main(String[] args) throws Exception {
        // Versile license 
        Versile.setInternalUseAGPL();
        VersileMessagingClient vmc = new VersileMessagingClient("localhost");
        VRSAKeyPair key = VDecentralIdentity.dia(1024, "test", "peter", "dfw/23hcFDawqaDreq");
        VPrivateCredentials credentials = new VPrivateCredentials(key);
        vmc.connect(credentials);
        for(String msg : vmc.listMessages()) {
            System.out.println(msg);
        }
        vmc.saveMessageToFile("vmessage.txt",new File("vmessage.txt"));
        vmc.disconnect();
    }
}
