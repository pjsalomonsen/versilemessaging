/*
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.central;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.interfaces.RSAPublicKey;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.versile.common.auth.VAuth;
import org.versile.common.auth.VCredentials;
import org.versile.common.auth.VPrivateCredentials;
import org.versile.common.peer.VInetSocketPeer;
import org.versile.common.peer.VPeer;
import org.versile.crypto.VCryptoException;
import org.versile.crypto.VRSAKeyPair;
import org.versile.orb.entity.VProxy;
import org.versile.orb.link.VLinkAuth;
import org.versile.reactor.service.VOPService;
import org.versile.Versile;
import org.versile.orb.link.VLinkException;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class VersileMessagingVOPServiceManager {
    File messagesDir = new File("versilemessagestorage");
    LocalMessageNotification localMessageNotification = new LocalMessageNotification();
    HashSet<VProxy> clientListeners = (HashSet<VProxy>) new HashSet<VProxy>();
    static HashSet<RSAPublicKey> authorizedKeys = new HashSet<RSAPublicKey>();
    
    VOPService service;
    
    
    public VersileMessagingVOPServiceManager(VRSAKeyPair serverKey,InputStream authorizedKeysStream) throws VCryptoException, IOException, VLinkException {
        init(serverKey,authorizedKeysStream);
    }
    
    public VersileMessagingVOPServiceManager(VRSAKeyPair serverKey, InputStream authorizedKeysStream, String messagesDirPath) throws VCryptoException, IOException, VLinkException {
        messagesDir = new File(messagesDirPath);
        init(serverKey,authorizedKeysStream);
    }
    
    private void init(VRSAKeyPair serverkey, InputStream authorizedKeysStream) throws VCryptoException, IOException, VLinkException {           
        // Import authorized keys
        BufferedReader br = new BufferedReader(new InputStreamReader(authorizedKeysStream));
        String line = br.readLine();
        String keyString = "";
        boolean isKey = false;
        while(line!=null) {

            if(line.startsWith("-----BEGIN RSA PUBLIC KEY-----")) {
                isKey = true;
                keyString="";
            }
            if(isKey) {
                keyString+=line+"\n";
            }
            if(line.startsWith("-----END RSA PUBLIC KEY-----")) {
                authorizedKeys.add(VRSAKeyPair.importPublicArmored(keyString.getBytes()));
                Logger.getLogger(getClass().getName()).info("Imported authorized key");
            }
            line = br.readLine();
        }
        
        // Start Versile messaging
        
        VPrivateCredentials credentials = new VPrivateCredentials(serverkey);
        service = new VOPService(new VersileMessagingGateway.Factory(this), credentials, 
                new VLinkAuth(new VAuth(true,false) {

            @Override
            public synchronized boolean acceptCredentials(VCredentials credentials) {
                if(authorizedKeys.contains(credentials.getPublicKey())){
                    Logger.getLogger(getClass().getName()).info("Authorized link");
                    return true;
                } else {
                    Logger.getLogger(getClass().getName()).info("Unknown public key");
                    return false;
                }
            }
		
            @Override
            public synchronized boolean acceptPeer(VPeer peer) {
                VInetSocketPeer ipeer = (VInetSocketPeer)peer;
                Logger.getLogger(VersileMessaging.class.getName()).
                        log(Level.INFO, 
                        "Link auth request from {0}", 
                        ipeer.getInetAddress().getHostString());            
                return super.acceptPeer(peer);
            }                    
        }));
        
        if(!messagesDir.exists()) {
            messagesDir.mkdir();
        }
    }
    
    public void start() throws IOException {
        service.start();
    }
    
    public void stop() {
        service.stop(true, true);
    }

    public File getMessagesDir() {
        return messagesDir;
    }

    public LocalMessageNotification getLocalMessageNotification() {
        return localMessageNotification;
    }   
    
    public void notifyClientListeners(String fileName) {
        Iterator<VProxy> proxyIt = clientListeners.iterator();
        while(proxyIt.hasNext()) {
            VProxy cl = proxyIt.next();
            try {                
                cl.call("notifyClientListener", fileName);
            } catch (Exception ex) {
                Logger.getLogger(VersileMessagingVOPServiceManager.class.getName()).log(Level.SEVERE, null, ex);
                proxyIt.remove();
            }
        }
    }
}
