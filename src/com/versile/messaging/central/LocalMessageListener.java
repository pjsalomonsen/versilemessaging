/*
 * Copyright (c) 2012 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.central;

/**
 *
 * @author Peter Johan Salomonsen
 */
public interface LocalMessageListener {
    public void messageReceived(String filename);
}
