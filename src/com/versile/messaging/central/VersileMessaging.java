/*
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.central;


import java.io.File;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.versile.Versile;
import org.versile.crypto.VDecentralIdentity;

/**
 * Start Versile messaging server using defaults
 * 
 * @author Peter Johan Salomonsen
 */
public class VersileMessaging {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {     
        // Versile license 
        Versile.setInternalUseAGPL();
        File authKeys = new File("authorized_keys");
        final VersileMessagingVOPServiceManager vmm = new VersileMessagingVOPServiceManager(
                VDecentralIdentity.dia(1024, "VersileMessaging", "Versile", "Versile2014"),
                new FileInputStream("authorized_keys"));
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                Logger.getLogger(getClass().getName()).log(Level.INFO,"Received kill signal - will stop");
                vmm.stop();
                Logger.getLogger(getClass().getName()).log(Level.INFO,"Versile messaging terminated");
            }
            
        });
        vmm.start();
        Logger.getLogger(VersileMessaging.class.getName()).log(Level.INFO,"Versile messaging started");
        System.out.println("Versile messaging started");
    }
}
