/*
 * Copyright (c) 2012 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.central;

import java.io.File;
import org.versile.common.util.VExceptionProxy;
import org.versile.orb.entity.VCallError;
import org.versile.orb.entity.VObject;
import org.versile.orb.external.Publish;
import org.versile.orb.external.VExternal;
import org.versile.orb.service.VGatewayFactory;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class VersileMessagingGateway extends VExternal {
    
    VersileMessagingVOPServiceManager mgr;
    
    /**
    * Resolve relative VRI reference
    * 
    * @param obj path as per VRI standard
    * @return referenced resource
    * @throws VExceptionProxy invalid path
    * @throws VCallError illegal argument
    */
    @Publish(show=true, ctx=false)
    public Object urlget(Object obj) 
                   throws VExceptionProxy, VCallError {        
        MessageService msr = new MessageService();        
        msr.setServiceManager(mgr);
        return msr;
    }
    
    public static class Factory implements VGatewayFactory {
        VersileMessagingVOPServiceManager mgr;
        
        public Factory(VersileMessagingVOPServiceManager mgr) {
            this.mgr = mgr;
            
        }
                
        @Override
        public VObject build() {
            VersileMessagingGateway vmg = new VersileMessagingGateway();
            vmg.mgr = mgr;
            return vmg;
        }
    }
}
