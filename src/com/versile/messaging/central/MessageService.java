/*
 * Copyright (c) 2012 Versile AS
 * 
 * This file is part of Versile Messaging.
 * 
 *  Versile Messaging is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Versile Messaging is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero Public License for more details.
 *
 *  You should have received a copy of the GNU Affero Public License
 *  along with Versile Messaging.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.versile.messaging.central;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.versile.orb.entity.VProxy;
import org.versile.orb.external.Publish;
import org.versile.orb.external.VExternal;
import org.versile.vse.stream.VByteStreamer;
import org.versile.vse.stream.VSimpleFileStreamerData;
import org.versile.vse.stream.VSimpleFileStreamerData.Config;
import org.versile.vse.stream.VStreamError;

/**
 *
 * @author Peter Johan Salomonsen
 */
public class MessageService extends VExternal {   
    
    VersileMessagingVOPServiceManager serviceManager;
    
    public MessageService() {
    }

    
    private Set<String> getMessagesSet() {
        File[] files = serviceManager.getMessagesDir().listFiles();
        HashSet<String> fileNamesSet = new HashSet<String>();
        for(File file : files) {
            if(file.isFile()) {
                fileNamesSet.add(file.getName());
            }
        }
        return fileNamesSet;
    }
    
    @Publish(show=true, ctx=false)    
    public String[] listMessages() {                
        return getMessagesSet().toArray(new String[0]);
    }
    
    @Publish(show=true, ctx=false)    
    public VByteStreamer getMessage(String filename) throws VStreamError, FileNotFoundException {       
        Set<String> messagesFileNamesSet = getMessagesSet();
        if(messagesFileNamesSet.contains(filename)) {
            Config cfg = new Config();
            cfg.setReadable(true);
            Logger.getLogger(VersileMessaging.class.getName()).log(Level.INFO, "Provided stream for message {0}", filename);
            return new VByteStreamer(
                    new VSimpleFileStreamerData(
                    new RandomAccessFile(new File(serviceManager.getMessagesDir(),filename), "r"),
                    cfg));
        } else {
            throw new FileNotFoundException("Message not found");
        }
    }
    
    @Publish(show=true, ctx=false)    
    public VByteStreamer sendMessageAsStream(String filename) throws VStreamError, FileNotFoundException {       
        Config cfg = new Config();
        cfg.setWritable(true);
        cfg.setCanSeekFwd(true);
        cfg.setCanSeekRew(true);
        try {
	    File file = new File(serviceManager.getMessagesDir(),filename);
	    if(file.exists()) {
		file.delete();
	    }
            VSimpleFileStreamerData vsd = new VSimpleFileStreamerData(
                new RandomAccessFile(file, "rw"),
                cfg);
            
            VByteStreamer stream = new VByteStreamer(
                vsd);
            Logger.getLogger(VersileMessaging.class.getName()).log(Level.INFO, "Providing stream for message {0}", filename);
            return stream;
        } catch(Exception e) {
            Logger.getLogger(VersileMessaging.class.getName()).log(Level.SEVERE, "Error providing stream for message "+filename,e);
            return null;
        }
    }
    
    @Publish(show=true, ctx=false)
    public void sendMessage(String filename,byte[] messageBytes) {
        try {
            new FileOutputStream(new File(serviceManager.getMessagesDir(),filename)).write(messageBytes);            
        } catch (IOException ex) {
            Logger.getLogger(MessageService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Publish(show=true, ctx=false)
    public void registerClientListener(VProxy clientListener) {
        serviceManager.clientListeners.add(clientListener);
    }
    
    @Publish(show=true, ctx=false)
    public void notifyListeners(String filename) {
        serviceManager.getLocalMessageNotification().notifyLocalMessageListeners(filename);
    }
        
    @Publish(show=true, ctx=false)
    public Boolean deleteMessage(String filename) throws FileNotFoundException {
        Set<String> messagesFileNamesSet = getMessagesSet();
        if(messagesFileNamesSet.contains(filename)) {
            new File(serviceManager.getMessagesDir(),filename).delete();
            Logger.getLogger(VersileMessaging.class.getName()).log(Level.INFO, "Deleted message {0}", filename);
            return true;
        }
        else {
            throw new FileNotFoundException("Message not found");
        }
    }    

    public void setServiceManager(VersileMessagingVOPServiceManager serviceManager) {
        this.serviceManager = serviceManager;
    }   
}
